# Callables

PWD ?= $(shell 'pwd')

jq_get_from_file = $(shell cat $(1) | jq '$(shell echo $(2))')
jq_get_from_string = $(shell echo $(1) | jq '$(shell echo $(2))')
