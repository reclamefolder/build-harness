
TF_BACKEND_CONFIG_HASH_FILE ?= ".tf_backend_config_hash"
TF_STATE_CONFIG_PREFIX ?= "state_"

tf_add_backend_config_prefix = $(shell echo $(TF_STATE_CONFIG_PREFIX))$(shell echo $(1))
tf_backend_config_is_changed = $(if $(call file_exists,"$(shell echo $(TF_BACKEND_CONFIG_HASH_PATH))"),$(call tf_compare_backend_configuration),$(true))
tf_compare_backend_configuration = $(call sne,$(strip $(call hash_string,$(call tf_complete_backend_configuration))),$(call tf_get_previous_backend_config))
tf_complete_backend_configuration = $(call set_create,$(call map,TF_config_from_file_or_set,$(shell echo $(TF_BACKEND_CONFIG))))
tf_get_previous_backend_config = $(strip $(shell cat $(shell echo $(TF_BACKEND_CONFIG_HASH_PATH))))
rf_flag_backend_config = "-backend-config='$(shell echo $(1))'"
tf_set_backend_config_variables = $(if $(call strlen_gt,$(shell echo $(1)),1),$(call map,tf_add_backend_config_prefix,$(shell echo $(1))),)
