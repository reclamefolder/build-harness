.PHONY: tf/state/rm tf/state/list

tf/state/rm:
	$(call assert-set,TF_ADDRESS)
	@cd $(TF_DIRECTORY) && \
		$(TF) state rm $(TF_ADDRESS);

tf/state/list:
	@cd $(TF_DIRECTORY) && \
		$(TF) state list;
