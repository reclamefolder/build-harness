.PHONY: gcr/delete

gcr/delete:
	@gcloud container \
		images delete $(GCR_IMAGE_PATH):$(GCR_IMAGE_TAG) \
		$(if $(GCR_FORCE_DELETE),--force-delete-tags,);

gcr/list:
	$(call assert-set,GCR_REPOSITORY)
	@gcloud container \
		images list \
		--repository $(GCR_REPOSITORY) \
		$(if $(GCR_FILTER),--filter $(GCR_FILTER),) \
		$(if $(GCR_LIMIT),--limit $(GCR_LIMIT),) \
		$(if $(GCR_PAGE_SIZE),--limit $(GCR_PAGE_SIZE),) \
		$(if $(GCR_SORT_BY),--limit $(GCR_SORT_BY),);
