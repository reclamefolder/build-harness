WGET := $(shell which wget)

WGET_TMP_FILENAME := wget-$(call now_rfc3339).tmp

wget/unzip:
	wget -O ./$(WGET_TMP_FILENAME) $(WGET_URL) && \
		unzip -d $(WGET_DESTINATION) ./$(WGET_TMP_FILENAME) && \
		rm ./$(WGET_TMP_FILENAME)
