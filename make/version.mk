.PHONY: version/cleaned/semver version/current version/current/major version/current/minor version/current/patch version/current/prerelease version/set

PWD ?= $(shell 'pwd')
RF ?= $(shell which rf)

HAS_VERSION := $(shell test -e $(VERSION_PATH) && echo "1")

get_current_version = $(shell $(RF) version get $(1))
get_clean_semver = $(shell echo $(if $(1),$(shell echo $(1)),$(call get_current_version)) | egrep -oi '([0-9]{0,3}\.[0-9]{0,4}\.[0-9]{0,5})')
version_tag_exists = $(if $(shell git ls-remote $(GIT_REMOTE) refs/tags/$(if $1,$1,$(call get_current_version))),$(true),)

ifeq ($(HAS_VERSION),1)
	CURRENT_VERSION := "$(call get_current_version)"
else
	CURRENT_VERSION := "1.0.0"
endif

version/cleaned/semver:
	@echo $(call get_clean_semver,$(VERSION));

version/current:
	$(call assert-set,RF);
	@echo $(call get_current_version);

version/current/major:
	$(call assert-set,RF);
	@echo $(call get_current_version,major);

version/current/minor:
	$(call assert-set,RF);
	@echo $(call get_current_version,minor);

version/current/patch:
	$(call assert-set,RF);
	@echo $(call get_current_version,patch);

version/current/prerelease:
	$(call assert-set,RF);
	@echo $(call get_current_version,prerelease);

version/set:
	$(call assert-set,RF);
	$(call assert-set,VERSION);
	@rf version set $(VERSION);

version/tag/exists:
	@echo v$(call get_current_version) $(if $(call version_tag_exists),exists,not used);

version/validate:
ifeq ($(call version_tag_exists,$(VERSION)),$(true))
	@echo v$(if $(VERSION),$(VERSION),$(call get_current_version)) already exists;
	@echo "Update version";
	@exit 1;
else
	@echo "Version is valid";
endif