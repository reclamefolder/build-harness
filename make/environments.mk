.PHONY: environment/display

PWD ?= $(shell 'pwd')

ENVIRONMENT ?= "staging"

is_production := $(if $(call seq,$(shell echo $(ENVIRONMENT)),production),true,)
is_staging := $(if $(or $(call seq,$(shell echo $(ENVIRONMENT)),staging),$(call seq,$(shell echo $(ENVIRONMENT)),acceptance)),true,)
is_acceptance := $(if $(or $(call seq,$(shell echo $(ENVIRONMENT)),staging),$(call seq,$(shell echo $(ENVIRONMENT)),acceptance)),true,)
is_development := $(if $(and $(call not,$(is_production)),$(call not,$(is_acceptance))),true,)

is_not_production := $(if $(call not,$(is_production)),true,)
is_not_staging := $(if $(call not,$(is_staging)),true,)
is_not_acceptance := $(if $(call not,$(is_acceptance)),true,)
is_not_development := $(if $(call not,$(is_development)),true,)

environment/display:
	@echo "ENVIRONMENT = $(ENVIRONMENT)";
	@echo "is_production = $(is_production)";
	@echo "is_not_production = $(is_not_production)";
	@echo "is_staging = $(is_staging)";
	@echo "is_not_staging = $(is_not_staging)";
	@echo "is_acceptance = $(is_acceptance)";
	@echo "is_not_acceptance = $(is_not_acceptance)";
	@echo "is_development = $(is_development)";
	@echo "is_not_development = $(is_not_development)";
