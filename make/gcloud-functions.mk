.PHONY: gc/functions/deploy

PWD ?= $(shell 'pwd')

GC_FUNCTION_TRIGGER ?= "http"
GC_FUNCTION_REGION ?= "europe-west1"
GC_FUNCTION_RUNTIME ?= "nodejs10"
GC_FUNCTION_MEMORY ?= 256
GC_FUNCTION_TIMEOUT ?= 540
GC_FUNCTION_VERBOSITY ?= $(GC_VERBOSITY)

gc_function_env_var_separator = ,

gc_function_merge_env_vars = $(call merge,$(gc_function_env_var_separator),$(1))

ifeq ($(if $(or $(GC_FUNCTION_ENV_VARS),$(GC_FUNCTION_ENV_VARS_RAW)),true,),true)
	gc_function_has_env_vars = "true"
	GC_FUNCTION_ENV_VARS_FLAG := --set-env-vars $(call gc_function_merge_env_vars,$(shell echo $(GC_FUNCTION_ENV_VARS)))$(if $(and $(GC_FUNCTION_ENV_VARS),$(GC_FUNCTION_ENV_VARS_RAW)),$(gc_function_env_var_separator),)$(GC_FUNCTION_ENV_VARS_RAW)
else
	gc_function_has_no_env_vars = "true"
	GC_FUNCTION_ENV_VARS_FLAG := ""
endif

define gc_function_deploy_command
gcloud functions deploy $(GC_FUNCTION_NAME) \
	--region $(GC_FUNCTION_REGION) \
	--memory $(GC_FUNCTION_MEMORY) \
	--runtime $(GC_FUNCTION_RUNTIME) \
	--source $(GC_FUNCTION_SOURCE) \
	--entry-point $(GC_FUNCTION_ENTRY_POINT) \
	--timeout $(GC_FUNCTION_TIMEOUT) \
	$(GC_FUNCTION_ENV_VARS_FLAG) \
	--trigger-$(shell echo $(GC_FUNCTION_TRIGGER)) \
	--verbosity $(GC_FUNCTION_VERBOSITY)
endef

gc/functions/deploy:
	$(gc_function_deploy_command);

gc/functions/deploy/command:
	@echo $(gc_function_deploy_command);

gc/functions/environmentVariables:
	@echo $(if $(or $(GC_FUNCTION_ENV_VARS),$(GC_FUNCTION_ENV_VARS_RAW)),true,);
	@echo gc_function_has_env_vars=$(gc_function_has_env_vars);
	@echo $(GC_FUNCTION_ENV_VARS_FLAG);
