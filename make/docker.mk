.PHONY: docker/build docker/push docker/tag docker/version

DOCKER := $(shell which docker)

PWD ?= $(shell 'pwd')

DOCKER_PACKAGE_PATH ?= $(PWD)

add_flag_build_arg ?= --build-arg $(shell echo $(1))
add_flag_file ?= --file $(shell echo $(1))

docker_tag_for_deployment_type = $(if $(is_production_deployment),latest,$(if $(is_staging_deployment),next,dev))

docker/build:
	$(call assert-set,DOCKER_IMAGE_NAME);
	$(call assert-set,DOCKER_IMAGE_TAG);
	$(call assert-set,DOCKER_PACKAGE_PATH);
	$(DOCKER) build \
		-t $(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG) \
		$(call map_add_flag,build_arg,$(DOCKER_BUILD_ARGS)) \
		$(call map_add_flag,file,$(DOCKER_IMAGE_FILE)) \
		$(DOCKER_PACKAGE_PATH);

docker/push:
	$(call assert-set,DOCKER_IMAGE_NAME);
	$(call assert-set,DOCKER_IMAGE_TAG);
	@$(DOCKER) push \
		$(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG);

docker/tag:
	$(call assert-set,DOCKER_IMAGE_NAME);
	$(call assert-set,DOCKER_IMAGE_TAG);
	$(call assert-set,DOCKER_SRC_IMAGE);
	@$(DOCKER) tag \
		$(DOCKER_SRC_IMAGE) \
		$(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG);

docker/version:
	@$(DOCKER) --version;
