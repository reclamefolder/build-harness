.PHONY: npm/install npm/install/production npm/login npm/publish npm/validate

PWD ?= $(shell 'pwd')

NPM_PUBLISH_TAG ?= $(if $(is_production_deployment),latest,next)

npm/install:
	@pnpm install --frozen-lockfile;

npm/install/production:
	@rm -rf ./node_modules;
	@pnpm install --frozen-lockfile --prod;

npm/login:
	$(call assert-set,NPM_TOKEN);
	@echo "//registry.npmjs.org/:_authToken=$(NPM_TOKEN)" > .npmrc;

npm/publish:
	$(call assert-set,NPM_PUBLISH_TAG);
	@echo "TAG: $(shell echo $(NPM_PUBLISH_TAG))";
	pnpm publish --tag $(shell echo $(NPM_PUBLISH_TAG));

npm/validate:
	@pnpm audit;
