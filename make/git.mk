.PHONY: git/publishAllTags git/publishTag git/tag

PWD ?= $(shell 'pwd')

GIT_REMOTE ?= "origin"

GIT := $(shell which git)

git_last_commit_msg = $(shell git log -1 --pretty=%B)

git/commit:
	@git commit -am "$(shell echo $(GIT_MESSAGE))";

git/publishAllTags:
	@git push \
		$(GIT_REMOTE) \
		--tags;

git/publishTag:
	$(call assert-set,GIT_TAG);
	@git push \
		$(GIT_REMOTE) \
		$(GIT_TAG);

git/tag:
	$(call assert-set,GIT_TAG)
	$(call assert-set,GIT_MESSAGE)
	@git tag \
		-a $(shell echo $(GIT_TAG)) \
		-m "$(shell echo $(GIT_MESSAGE))";
