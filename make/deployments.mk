.PHONY: deployment/display

PWD ?= $(shell 'pwd')
DEPLOYMENT_FILE ?= deployment
DEPLOYMENT_PATH := $(PWD)/$(DEPLOYMENT_FILE)

get_deployment_from_file = $(if $(call file_exists,$(DEPLOYMENT_PATH)),$(shell cat $(DEPLOYMENT_PATH)),staging)
set_deployment_to_file = $(shell echo $(1) > $(DEPLOYMENT_PATH))

is_production_deployment := $(if $(call seq,$(shell echo $(DEPLOYMENT)),production),true,)
is_staging_deployment := $(if $(or $(call seq,$(shell echo $(DEPLOYMENT)),staging),$(call seq,$(shell echo $(DEPLOYMENT)),acceptance)),true,)
is_acceptance_deployment := $(if $(or $(call seq,$(shell echo $(DEPLOYMENT)),staging),$(call seq,$(shell echo $(DEPLOYMENT)),acceptance)),true,)
is_development_deployment := $(if $(and $(call not,$(is_production_deployment)),$(call not,$(is_acceptance_deployment))),true,)

is_not_production_deployment := $(if $(call not,$(is_production_deployment)),true,)
is_not_staging_deployment := $(if $(call not,$(is_staging_deployment)),true,)
is_not_acceptance_deployment := $(if $(call not,$(is_acceptance_deployment)),true,)
is_not_development_deployment := $(if $(call not,$(is_development_deployment)),true,)

deployment/display:
	@echo "DEPLOYMENT = $(DEPLOYMENT)";
	@echo "is_production_deployment = $(is_production_deployment)";
	@echo "is_not_production_deployment = $(is_not_production_deployment)";
	@echo "is_staging_deployment = $(is_staging_deployment)";
	@echo "is_not_staging_deployment = $(is_not_staging_deployment)";
	@echo "is_acceptance_deployment = $(is_acceptance_deployment)";
	@echo "is_not_acceptance_deployment = $(is_not_acceptance_deployment)";
	@echo "is_development_deployment = $(is_development_deployment)";
	@echo "is_not_development_deployment = $(is_not_development_deployment)";

deployment/set:
	$(call assert-set,SET_DEPLOYMENT);
	@echo "DEPLOYMENT = $(SET_DEPLOYMENT)";
	$(call set_deployment_to_file,$(SET_DEPLOYMENT))
	@echo "DEPLOYMENT is SET";
