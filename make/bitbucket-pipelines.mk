.PHONY: bitbucket/branch
PWD ?= $(shell 'pwd')

ifdef BITBUCKET_PR_ID
	is_pr_pipeline := $(true)
else
	is_not_pr_pipeline := $(true)
endif
ifdef BITBUCKET_TAG
	is_tag_pipeline := $(true)
else
	is_not_tag_pipeline := $(true)
endif

ifneq ("$(and $(is_not_pr_pipeline),$(is_not_tag_pipeline),$(BITBUCKET_BRANCH))","")
	is_branch_pipeline := $(true)
else
	is_not_branch_pipeline := $(true)
endif

is_production_branch := $(if $(call seq,$(shell echo $(BITBUCKET_BRANCH)),production),$(true),)
is_branch_staging := $(if $(call seq,$(shell echo $(BITBUCKET_BRANCH)),staging),$(true),)
is_branch_acceptance := $(if $(or $(call seq,$(shell echo $(BITBUCKET_BRANCH)),acceptance),$(call seq,$(shell echo $(BITBUCKET_BRANCH)),accep)),$(true),)
is_staging_branch := $(if $(or $(is_branch_acceptance),$(is_branch_staging)),$(true),)
is_acceptance_branch := $(if $(or $(is_branch_acceptance),$(is_branch_staging)),$(true),)
is_development_branch := $(if $(and $(call not,$(is_production_branch)),$(call not,$(is_acceptance_branch))),$(true),)

is_not_production_branch := $(if $(call not,$(is_production_branch)),$(true),)
is_not_staging_branch := $(if $(call not,$(is_staging_branch)),$(true),)
is_not_acceptance_branch := $(if $(call not,$(is_acceptance_branch)),$(true),)
is_not_development_branch := $(if $(call not,$(is_development_branch)),$(true),)

bitbucket/branch:
	@echo is_branch_pipeline = $(if $(is_branch_pipeline),true, false)
	@echo is_not_branch_pipeline = $(if $(is_not_branch_pipeline),true, false)
	@echo "";
	@echo is_production_branch = $(if $(is_production_branch),true, false)
	@echo is_staging_branch = $(if $(is_staging_branch),true, false)
	@echo is_acceptance_branch = $(if $(is_acceptance_branch),true, false)
	@echo is_development_branch = $(if $(is_development_branch),true, false)
	@echo "";
	@echo is_not_production_branch = $(if $(is_not_production_branch),true, false)
	@echo is_not_staging_branch = $(if $(is_not_staging_branch),true, false)
	@echo is_not_acceptance_branch = $(if $(is_not_acceptance_branch),true, false)
	@echo is_not_development_branch = $(if $(is_not_development_branch),true, false)
	@echo "";
