.PHONY: gc/auth/serviceAccount/login

PWD ?= $(shell 'pwd')
GC_APPLICATION_CREDENTIALS ?= "client-secret.json"

gc/auth/serviceAccount/login:
ifeq ($(GC_CLIENT_SECRET),)
	@echo "No Client Secret set, using: $(GC_APPLICATION_CREDENTIALS) contents";
else
	@printf "%s" '${GC_CLIENT_SECRET}' > "$(shell echo $(GC_APPLICATION_CREDENTIALS))";
endif
	@gcloud auth activate-service-account --key-file "$(shell echo $(GC_APPLICATION_CREDENTIALS))";
