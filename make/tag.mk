.PHONY: tag/develop tag/playground tag/production tag/release

PWD ?= $(shell 'pwd')

TAG_BRANCH ?= "PG"
TAG_BUILD_NUMBER ?= "0"

get_version_tag = $(if $(shell echo $(TAG_PREFIX)),$(shell echo $(TAG_PREFIX)),)$(if $(is_production_deployment),$(call get_current_rf_version),$(call get_current_rf_version)-$(shell echo $(1)).$(shell echo $(TAG_BUILD_NUMBER)))$(if $(shell echo $(TAG_SUFFIX)),-$(shell echo $(TAG_SUFFIX)),)

tag/develop:
	$(call assert-set,TAG_BUILD_NUMBER)
	@make git/tag \
		GIT_TAG=$(call get_version_tag,DEV) \
		GIT_MESSAGE="[$(shell echo $(TAG_BRANCH))][$(shell echo $(TAG_BUILD_NUMBER))] v$(shell echo $(call get_version_tag,DEV))";
	@make git/publishTag \
		GIT_TAG=$(call get_version_tag,DEV);

tag/playground:
	$(call assert-set,TAG_BUILD_NUMBER)
	@make git/tag \
		GIT_TAG=$(call get_version_tag,PG) \
		GIT_MESSAGE="[$(shell echo $(TAG_BRANCH))][$(shell echo $(TAG_BUILD_NUMBER))] v$(shell echo $(call get_version_tag,PG))";
	@make git/publishTag \
		GIT_TAG=$(call get_version_tag,PG);

tag/production:
	$(call assert-set,TAG_BUILD_NUMBER)
	@make git/tag \
		GIT_TAG=$(call get_version_tag) \
		GIT_MESSAGE="[MASTER][$(shell echo $(TAG_BUILD_NUMBER))] v$(call get_current_rf_version)";
	@make git/publishTag \
		GIT_TAG=$(call get_version_tag);

tag/release:
	$(call assert-set,TAG_BUILD_NUMBER)
	@make git/tag \
		GIT_TAG=$(call get_version_tag,RC) \
		GIT_MESSAGE="[RELEASE][$(shell echo $(TAG_BUILD_NUMBER))] v$(shell echo $(call get_version_tag,RC))";
	@make git/publishTag \
		GIT_TAG=$(call get_version_tag,RC);

tag/test:
	@echo $(call get_version_tag,$(ADDITION))
