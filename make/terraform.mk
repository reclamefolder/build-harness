.PHONY: tf/apply tf/fmt tf/init tf/lint tf/plan tf/validate tf/workspace

PWD ?= $(shell 'pwd')
TF := $(shell which terraform)

TF_GRAPH_TYPE ?= plan
TF_GRAPH_MODULE_DEPTH ?= -1
TF_LOG ?= "WARN"
TF_DIRECTORY ?= "terraform/"
TF_OUT ?= "terraform.tfplan"
TF_VAR_FLAGS ?= ""
TF_WORKSPACE ?= "default"
TF_PARALLELISM ?= 10
TF_VARS ?= ""
TF_VAR_FILES ?= ""
TF_DESTROY ?= "false"

TF_flag_target = '-target "$(shell echo $(1))"'
TF_flag_var_file = "-var-file $(shell echo $(1))"
TF_flag_var = "-var '$(shell echo $(1))'"
TF_flag_backend_config = "-backend-config=\"$(shell echo $(1))\""

TF_PATH := $(PWD)/$(TF_DIRECTORY)

TF_ENV_VAR_FILE := "$(ENVIRONMENT).tfvars"
TF_VAR_FILE_FLAG := "-var-file=$(TF_VAR_FILE)"

ifeq ($(shell echo $(TF_DESTROY)),true)
	TF_DESTROY_FLAG = "-destroy"
else
	TF_DESTROY_FLAG = ""
endif

# Callables
TF_add_flags = $(if $(call strlen_gt,$(shell echo $(2)),1),$(call map,TF_flag_$(1),$(shell echo $(2))),"")
tf_output = $(shell cd $(TF_DIRECTORY)/ && terraform output $(1))
tf_output_json = $(shell cd $(TF_DIRECTORY)/ && terraform output -json $(1))

tf/apply:
	$(call assert-set,TF_OUT);
	@echo "Apply Terraform plan \"$(TF_OUT)\" for \"$(TF_WORKSPACE)\"";
	@cd $(TF_DIRECTORY) && \
		$(TF) apply \
			-input=false \
			-refresh=true \
			-parallelism=$(TF_PARALLELISM) \
			"$(shell echo $(TF_OUT))";
	@echo "";

tf/fmt:
	@echo "Rewrite Terraform configuration files to a canonical format and style";
	@cd $(TF_DIRECTORY) && \
		$(TF) fmt \
			-write=true \
			-list=true \
			-diff=true;
	@echo "";

tf/graph:
	@cd $(TF_DIRECTORY) && \
		$(TF) graph \
			-type=$(TF_GRAPH_TYPE) \
			-module_depth=$(TF_GRAPH_MODULE_DEPTH) \
			$(if $(PIPE),| $(PIPE),)

tf/import:
	@echo "Import $() to Terraform";
	@cd $(TF_DIRECTORY) && \
		$(TF) import \
			-input=false \
			-parallelism=$(TF_PARALLELISM) \
			$(shell echo $(call TF_add_flags,var_file,$(TF_VAR_FILES))) \
	  		$(shell echo $(call TF_add_flags,var,$(TF_VARS))) \
			'$(shell echo $(TF_ADDRESS))' \
			'$(shell echo $(TF_ID))';

tf/init:
	@echo "Ensure clean Terraform Directory";
	@rm -rf $(TF_DIRECTORY)/.terraform;
	@echo "Init Terraform";
	@cd $(TF_DIRECTORY) && \
		TF_WORKSPACE="" \
		$(TF) init \
			-no-color \
			-force-copy \
			-get-plugins=true \
			-upgrade=true \
			$(if $(call seq,false,$(shell echo $(TF_BACKEND_CONFIG))),-backend=false,$(shell echo $(call TF_add_flags,backend_config,$(TF_BACKEND_CONFIG))));
	@echo "";

tf/lint:
	@echo "Lint Terraform";
	@cd $(TF_DIRECTORY) && \
		FAIL=`$(TF) fmt -write=false | xargs --no-run-if-empty -n 1 printf '\t- %s\n'`; \
		[ -z "$$FAIL" ] || (echo "Terraform configuration needs linting. Run 'make tf/fmt'"; echo $$FAIL; exit 1)
	@echo "";

tf/output:
	@echo '$(if $(call seq,$(TF_FORMAT),JSON),$(call tf_output_json,$(TF_OUTPUT_ID)),$(call tf_output,$(TF_OUTPUT_ID)))' \
		$(if $(call strlen_gt,$(TF_FILE),0),> $(TF_FILE),);

tf/plan: tf/init tf/workspace
	$(call assert-set,TF_OUT);
	@echo "Make Terraform plan for \"$(TF_WORKSPACE)\"";
	@cd $(TF_DIRECTORY) && \
		$(TF) plan \
			-refresh=true \
			-input=false \
      		-parallelism=$(TF_PARALLELISM) \
			$(shell echo $(TF_DESTROY_FLAG)) \
			$(shell echo $(call TF_add_flags,target,$(TF_TARGETS))) \
			$(shell echo $(call TF_add_flags,var_file,$(TF_VAR_FILES))) \
	  		$(shell echo $(call TF_add_flags,var,$(TF_VARS))) \
			-out="$(shell echo $(TF_OUT))";
	@echo "";

tf/planTest:
	@echo "";
	@echo "RESULT:";
	@echo "terraform plan \\";
	@echo "	-refresh=true \\";
	@echo "	-input=false \\";
	@echo "	-parallelism=$(TF_PARALLELISM) \\";
	@echo "	$(call TF_add_flags,var,$(TF_VARS)) \\";
	@echo "	$(call TF_add_flags,var_file,$(TF_VAR_FILES)) \\";
	@echo "	$(call TF_add_flags,target,$(TF_TARGETS)) \\";
	@echo "	-out="$(shell echo $(TF_OUT))";";
	@echo "";

tf/taint:
	@cd $(TF_DIRECTORY) && \
		$(TF) taint \
			$(TF_ADDRESS);
	@echo "";

tf/validate: tf/init
	@cd $(TF_DIRECTORY) && \
		$(TF) validate;
	@echo "";

tf/workspace:
	@echo "Selecting Terraform Workspace \"$(TF_WORKSPACE)\"";
	@cd "$(TF_DIRECTORY)" && \
		echo "yes" | TF_WORKSPACE="" $(TF) workspace select "$(TF_WORKSPACE)" || \
			$(TF) workspace new "$(TF_WORKSPACE)";
	@echo "";

tf/version:
	@$(TF) --version
