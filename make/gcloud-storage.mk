.PHONY: gc/storage/cp gc/storage/ls

gc_lc_bucket = $(shell gsutil ls $(shell echo $(1)))

gc/storage/cp:
	$(call assert-set,GC_STORAGE_SRC);
	$(call assert-set,GC_STORAGE_DEST);
	@gsutil cp $(shell echo $(GC_STORAGE_SRC)) $(shell echo $(GC_STORAGE_DEST));

gc/storage/ls:
	$(call assert-set,GC_BUCKET_URL);
	$(call gc_lc_bucket,$(GC_BUCKET_URL));
