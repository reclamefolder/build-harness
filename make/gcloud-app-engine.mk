.PHONY: gc/ae/deploy gc/ae/test

PWD ?= $(shell pwd);

GAE_CONFIG_DIR ?= "app-engine"
GAE_CONFIG_PATH := $(PWD)/$(shell echo $(GAE_CONFIG_DIR))

app_yaml_file_exists = $(call file_exists,"$(GAE_CONFIG_PATH)/$(shell echo $(1))")

get_production_app_yaml_options = "$(if $(is_production_deployment),"app-$(shell echo $(DEPLOYMENT))-$(shell echo $(DEPLOYMENT_TYPE))-v$(call get_current_rf_version_type,major).yaml app-$(shell echo $(DEPLOYMENT))-v$(call get_current_rf_version_type,major).yaml app-$(shell echo $(DEPLOYMENT))-$(shell echo $(DEPLOYMENT_TYPE)).yaml",)"

get_staging_app_yaml_options = "$(if $(is_staging_deployment),"app-$(shell echo $(DEPLOYMENT))-v$(call get_current_rf_version_type,major).yaml",)"

get_branch_yaml_options = "$(if $(shell echo $(BRANCH)),"app-$(shell echo $(BRANCH)).yaml","")"

get_app_yaml_options = $(call set_create,$(shell echo "$(call get_production_app_yaml_options) $(call get_staging_app_yaml_options) $(call get_branch_yaml_options) app-$(shell echo $(DEPLOYMENT)).yaml app.yaml" | sed 's/ \{1,\}/ /g'))

get_existing_app_yaml_files = $(call map,app_yaml_file_exists,$(call get_app_yaml_options))

get_app_yaml_location = $(call first,$(call get_existing_app_yaml_files))

get_save_ae_version = $(shell echo $(1) | sed -e 's/\./-/g' | tr A-Z a-z)

gc/ae/deploy:
	$(call assert-set,GAE_CONFIG_DIR);
	$(call assert-set,GAE_PROJECT_ID);
	$(call assert-set,GAE_VERSION);
	@echo "":
	@echo "DEPLOY v$(GAE_VERSION) to Project: $(GAE_PROJECT_ID)";
	@echo 'Executing with client: $(shell cat $(GC_APPLICATION_CREDENTIALS) | jq .client_email)';
	@echo "CONFIG = $(if $(shell echo $(GAE_CONFIG)),$(shell echo $(GAE_CONFIG)),$(call get_app_yaml_location))";
	gcloud app deploy $(if $(shell echo $(GAE_CONFIG)),$(shell echo $(GAE_CONFIG)),$(call get_app_yaml_location)) \
		--quiet \
		--project $(GAE_PROJECT_ID) \
		$(if $(shell echo $(GAE_IMAGE_URL)),--image-url $(shell echo $(GAE_IMAGE_URL)),) \
		-v $(call get_save_ae_version,$(shell echo $(GAE_VERSION))) \
		$(if $(shell echo $(GAE_PROMOTE)),--promote $(if $(shell echo $(GAE_STOP_PREV_VERSION)),--stop-previous-version,--no-stop-previous-version),--no-promote);

gc/ae/test:
	@echo "";
	@echo "gcloud app deploy $(if $(shell echo $(GAE_CONFIG)),$(shell echo $(GAE_CONFIG)),$(call get_app_yaml_location)) \\";
	@echo "  --quiet \\";
	@echo "  --project $(GAE_PROJECT_ID) \\";
	@echo "  $(if $(shell echo $(GAE_IMAGE_URL)),--image-url $(shell echo $(GAE_IMAGE_URL)),) \\";
	@echo "  -v $(call get_save_ae_version,$(shell echo $(GAE_VERSION))) \\";
	@echo "  $(if $(shell echo $(GAE_PROMOTE)),--promote $(if $(shell echo $(GAE_STOP_PREV_VERSION)),--stop-previous-version,--no-stop-previous-version),--no-promote)";
