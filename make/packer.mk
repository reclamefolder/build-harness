.PHONY: packer/build packer/inspect packer/validate packer/validateBuild packer/test

PWD ?= $(shell 'pwd')

PACKER := $(shell which packer)

PACKER_MACHINE_READABLE ?= "false"
PACKER_TEMPLATE ?= "build.json"
PACKER_VAR_FILE ?= ""
PACKER_VAR_FLAGS ?= ""

PACKER_flag_var_file = "-var-file $(shell echo $(1))"
PACKER_flag_var = "-var '$(shell echo $(1))'"

PACKER_add_flags = $(if $(call strlen_gt,$(shell echo $(2)),1),$(call map,PACKER_flag_$(1),$(shell echo $(2))),"")

ifeq ($(shell echo $(PACKER_MACHINE_READABLE)),true)
	PACKER_MACHINE_FLAG ?= "-machine-readable"
endif

packer/build: packer/validateBuild
	$(call assert-set,PACKER_TEMPLATE);
	@$(PACKER) $(shell echo $(PACKER_MACHINE_FLAG)) \
		build \
		$(shell echo $(call PACKER_add_flags,var_file,$(PACKER_VAR_FILES))) \
		$(shell echo $(call PACKER_add_flags,var,$(PACKER_VARS))) \
		$(shell echo $(PACKER_TEMPLATE));

packer/inspect:
	@$(PACKER) $(shell echo $(PACKER_MACHINE_FLAG)) \
		inspect \
		$(shell echo $(PACKER_TEMPLATE));

packer/validate:
	@$(PACKER) $(shell echo $(PACKER_MACHINE_FLAG))\
		validate \
		-syntax-only \
		$(shell echo $(PACKER_TEMPLATE));

packer/validateBuild:
	$(call assert-set,PACKER_TEMPLATE);
	@$(PACKER) $(shell echo $(PACKER_MACHINE_FLAG)) \
		validate \
		$(shell echo $(call PACKER_add_flags,var_file,$(PACKER_VAR_FILES))) \
		$(shell echo $(call PACKER_add_flags,var,$(PACKER_VARS))) \
		$(shell echo $(PACKER_TEMPLATE));

packer/testBuild:
	@echo "";
	@echo "RESULT:";
	@echo "$(PACKER) $(shell echo $(PACKER_MACHINE_FLAG)) \\"
	@echo "  build \\";
	@echo "  $(shell echo $(call PACKER_add_flags,var_file,$(PACKER_VAR_FILES))) \\";
	@echo "  $(shell echo $(call PACKER_add_flags,var,$(PACKER_VARS))) \\";
	@echo "  $(shell echo $(PACKER_TEMPLATE))";

packer/version:
	@$(PACKER) --version;
