
PWD ?= $(shell 'pwd')

RF_RELEASE_TYPE ?= "patch"

get_current_rf_version = $(shell rf version get)
get_current_rf_version_type = $(shell rf version get $(shell echo $(1)))

get_rf_config_environment_flag = $(if $(shell echo $(1)),--environment $(shell echo $(1)))
get_rf_config_local_flag = $(if $(or $(shell echo $(RF_CONFIG_LOCAL)),$(shell echo $(1))),--local,)
get_rf_config_projectid_flag = $(if $(shell echo $(GAE_PROJECT_ID)),--projectId $(shell echo $(GAE_PROJECT_ID)),)
get_rf_config_credentials_flag = $(if $(shell echo $(GC_APPLICATION_CREDENTIALS)),--credentials $(shell echo $(GC_APPLICATION_CREDENTIALS)),)

get_rf_config = $(shell rf config get $(shell echo $(1)) $(call get_rf_config_environment_flag,$(2)) $(call get_rf_config_local_flag,$(3)) $(call get_rf_config_projectid_flag) $(call get_rf_config_credentials_flag) --json)

rf/version:
	@echo $(call get_current_rf_version)

rf/version/major:
	@echo $(call get_current_rf_version_type,major)

rf/version/minor:
	@echo $(call get_current_rf_version_type,minor)

rf/version/patch:
	@echo $(call get_current_rf_version_type,patch)

rf/release:
	$(call assert-set,RF_RELEASE_TYPE);
	@rf release create $(shell echo $(RF_RELEASE_TYPE)) -yp

rf/config:
	$(call assert-set,RF_SERVICE_NAME)
	@echo $(call get_rf_config,$(shell echo $(RF_SERVICE_NAME)))
