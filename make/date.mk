now_rfc3339 = $(shell date -u +'%Y-%m-%dT%H:%M:%SZ')

date/now:
	@echo $(call now_rfc3339)
