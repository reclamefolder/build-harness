.PHONY: gc/sql/backup

gc_get_instance_id_from_host = $(call last,$(call split,:,$(shell echo $(1))))
gc_get_backups_for_instance_id = $(shell gcloud sql backups list --instance $(1) --limit $(if $(2),$(2),1) --sort-by "$date-field" --format json)

gc/sql/backup:
	$(call assert-set,GC_SQL_INSTANCE_ID)
	@gcloud sql backups create \
		--instance="$(GC_SQL_INSTANCE_ID)" \
		$(if $(shell echo $(GC_SQL_DESCRIPTION)),--description="$(shell echo $(GC_SQL_DESCRIPTION))",) \
		$(if $(shell echo $(GC_SQL_ASYNC)),--async,);

gc/sql/backup/restore:
	$(call assert-set,GC_SQL_BACKUP_ID)
	$(call assert-set,GC_SQL_INSTANCE_ID)
	$(call assert-set,GC_SQL_BACKUP_INSTANCE_ID)
	@gcloud sql backups restore \
		$(GC_SQL_BACKUP_ID) \
		--quiet \
		--restore-instance $(GC_SQL_INSTANCE_ID) \
		--backup-instance $(GC_SQL_BACKUP_INSTANCE_ID) \
		$(if $(shell echo $(GC_SQL_ASYNC)),--async,);
