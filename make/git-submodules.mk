.PHONY: git/submodule/init git/submodule/update

GIT_SM_JOBS ?= 4

git/submodule/init:
	@git submodule init;

git/submodule/update:
	@git submodule update --init --recursive --jobs $(GIT_SM_JOBS);
