.PHONY: flyway/migrate

FLYWAY ?= $(shell which flyway)

get_jdbc_url = "jdbc:$(shell echo $(1))://$(shell echo $(2))/$(shell echo $(3))"

flyway/migrate:
	$(call assert-set,FLYWAY)
	$(call assert-set,FLYWAY_URL)
	$(call assert-set,FLYWAY_USER)
	$(call assert-set,FLYWAY_PASSWORD)
	$(call assert-set,FLYWAY_SCHEMA)
	$(call assert-set,FLYWAY_FILE_LOCATIONS)
	@$(FLYWAY) \
		-url="$(shell echo $(FLYWAY_URL))" \
		-user="$(shell echo $(FLYWAY_USER))" \
		-password="$(shell echo $(FLYWAY_PASSWORD))" \
		-schemas="$(shell echo $(FLYWAY_SCHEMA))" \
		-locations="$(shell echo $(FLYWAY_FILE_LOCATIONS))" \
		migrate;

flyway/validate:
	$(call assert-set,FLYWAY)
	$(call assert-set,FLYWAY_URL)
	$(call assert-set,FLYWAY_USER)
	$(call assert-set,FLYWAY_PASSWORD)
	$(call assert-set,FLYWAY_SCHEMA)
	$(call assert-set,FLYWAY_FILE_LOCATIONS)
	@$(FLYWAY) \
		-url="$(shell echo $(FLYWAY_URL))" \
		-user="$(shell echo $(FLYWAY_USER))" \
		-password="$(shell echo $(FLYWAY_PASSWORD))" \
		-schemas="$(shell echo $(FLYWAY_SCHEMA))" \
		-locations="$(shell echo $(FLYWAY_FILE_LOCATIONS))" \
		validate;
