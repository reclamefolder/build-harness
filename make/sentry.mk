PWD ?= $(shell 'pwd')
SENTRY ?= $(if $(USE_NPX),npx -p @sentry/cli $(if $(SENTRY_NOINSTALL),--no-install,) sentry-cli,node_modules/.bin/sentry-cli)

.PHONY: sentry/newRelease sentry/release/new sentry/release/setCommits sentry/release/files/uploadSourceMaps sentry/release/finalize

sentry/newRelease:
	$(call assert-set,VERSION);
	$(call assert-set,SOURCEMAP_PATHS);
	@make sentry/release/new VERSION=$(VERSION) SENTRY_NOINSTALL=$(SENTRY_NOINSTALL);
	@make sentry/release/setCommits VERSION=$(VERSION) SENTRY_NOINSTALL=$(SENTRY_NOINSTALL);
	@for path in $(shell echo $(SOURCEMAP_PATHS)); do \
		make sentry/release/files/uploadSourceMaps PATH=$$path VERSION=$(VERSION) SENTRY_NOINSTALL=$(SENTRY_NOINSTALL); \
	done;
	@make sentry/release/finalize SENTRY_NOINSTALL=$(SENTRY_NOINSTALL); 

sentry/release/new:
	$(call assert-set,VERSION);
	$(SENTRY) releases new $(VERSION);

sentry/release/setCommits:
	$(call assert-set,VERSION);
	$(SENTRY) releases set-commits --auto $(VERSION);

sentry/release/files/uploadSourceMaps:
	$(call assert-set,VERSION);
	$(call assert-set,PATH);
	$(SENTRY) releases files $(VERSION) upload-sourcemaps $(PATH) --validate;

sentry/release/finalize:
	$(call assert-set,VERSION);
	$(SENTRY) releases finalize $(VERSION);

.PHONY: sentry/test/newRelease sentry/test/release/new sentry/test/release/setCommits sentry/test/release/files/uploadSourceMaps sentry/test/release/finalize

sentry/test/newRelease:
	$(call assert-set,VERSION);
	$(call assert-set,SOURCEMAP_PATHS);
	@make sentry/release/new SENTRY="echo $(SENTRY)" VERSION=$(VERSION);
	@make sentry/release/setCommits SENTRY="echo $(SENTRY)" VERSION=$(VERSION);
	@for path in $(shell echo $(SOURCEMAP_PATHS)); do \
		make sentry/release/files/uploadSourceMaps SENTRY="echo $(SENTRY)" PATH=$$path VERSION=$(VERSION); \
	done;
	@make SENTRY="echo $(SENTRY)" sentry/release/finalize; 

sentry/test/release/new:
	@make sentry/release/new SENTRY="echo $(SENTRY)" VERSION=$(VERSION);

sentry/test/release/setCommits:
	@make sentry/release/setCommits SENTRY="echo $(SENTRY)" VERSION=$(VERSION);

sentry/test/release/files/uploadSourceMaps:
	@make sentry/release/files/uploadSourceMaps SENTRY="echo $(SENTRY)" PATH=$$path VERSION=$(VERSION);

sentry/test/release/finalize:
	@make SENTRY="echo $(SENTRY)" sentry/release/finalize VERSION=$(VERSION);


.PHONY: sentry/deploy

sentry/deploy:
	$(call assert-set,VERSION);
	$(call assert-set,DEPLOYMENT);
	@$(SENTRY) releases deploys $(VERSION) new -e $(DEPLOYMENT);


.PHONY: sentry/test/deploy

sentry/test/deploy:
	@make SENTRY="echo $(SENTRY)" sentry/deploy VERSION=$(VERSION) DEPLOYMENT=$(DEPLOYMENT);
