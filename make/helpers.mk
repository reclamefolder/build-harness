define assert-set
  @[ -n "$($1)" ] || (echo "$(1) not defined in $(@)"; exit 1)
endef

define assert-unset
  @[ -z "$($1)" ] || (echo "$(1) should not be defined in $(@)"; exit 1)
endef

define assert-strlen-max
	@[ $(call strlen,$(1)) -lt $2 ] || (echo "String: \"$(1)\" is longer then $(2) characters ($(call strlen,$(1)) chars)"; exit 1;)
endef

define assert-strlen-min
	@[ $(call strlen,$(1)) -gt $2 ] || (echo "String: \"$(1)\" is longer then $(2) characters"; exit 1;)
endef

strlen_gt = $(call gt,$(call strlen,$(shell echo $(1))),$(2))

file_exists = $(wildcard $(shell echo $(1)))

map_add_flag = $(if $(call strlen_gt,$(shell echo $(2)),1),$(call map,add_flag_$(1),$(shell echo $(2))),)

default::
	@exit 0;
