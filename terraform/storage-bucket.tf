
module "label" {
  source  = "cloudposse/label/null"
  version = "0.20.0"

  context = merge(module.core_state.label.context, {
    id_length_limit = null
  })

  environment = ""
  stage       = ""
  namespace   = "rf-support"
  name        = var.name
  tags        = merge(var.tags, { Version = var.release_version })
}

resource "google_storage_bucket" "bucket" {
  name          = module.label.id
  project       = module.core_state.project.id
  location      = var.bucket_location
  storage_class = var.storage_class

  labels = {
    for key, value in module.label.tags :
    lower(key) => substr(lower(replace(replace(value, " ", "-"), ".", "-")), 0, 64)
  }
}

resource "google_storage_bucket_acl" "acl" {
  bucket = google_storage_bucket.bucket.name

  predefined_acl = var.predefined_acl
}
