output "bucket_name" {
  value = google_storage_bucket.bucket.name
}

output "bucket_url" {
  value = google_storage_bucket.bucket.url
}

output "bucket_acl" {
  value = google_storage_bucket_acl.acl.predefined_acl
}
