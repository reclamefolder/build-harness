locals {
  split_version = split("-", var.release_version)
  store_version = length(local.split_version) == 1 ? var.release_version : format("%s-%s", local.split_version[1], local.split_version[0])
}

resource "null_resource" "upload_objects" {
  triggers = {
    bucket          = google_storage_bucket.bucket.url
    release_version = var.release_version
    path            = var.dir_path
  }

  provisioner "local-exec" {
    command = "gsutil -m rsync -d -r ${var.dir_path} ${google_storage_bucket.bucket.url}/${local.store_version}"
  }
}
