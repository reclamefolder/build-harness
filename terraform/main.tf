terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.6.0"
    }
    null = {
      source = "hashicorp/null"
    }
  }
  backend "gcs" {
    prefix = "build-harness"
  }
  required_version = ">= 0.14"
}

module "core_state" {
  source = "./modules/rf-core-state"

  bucket = var.core_state_bucket
}
