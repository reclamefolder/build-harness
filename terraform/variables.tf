variable "core_state_bucket" {
  type        = string
  description = "The Bucket containing the Core State"
}

# ########## #
# LABEL DATA #
# ########## #

variable "name" {
  type        = string
  default     = ""
  description = "Name of the application"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Tags"
}

# #################### #
# BUCKET CONFIGURATION #
# #################### #

variable "bucket_location" {
  type        = string
  description = "The GCS location"
  default     = "europe-west1"
}

variable "storage_class" {
  type        = string
  description = "The Storage Class of the new bucket."
  default     = "REGIONAL"
}

variable "predefined_acl" {
  type        = string
  description = "The canned GCS ACL to apply"
  default     = "projectPrivate"
}

# ##################### #
# OBJECTS CONFIGURATION #
# ##################### #

variable "release_version" {
  type        = string
  description = "The SEMVER version for the files"
}

variable "dir_path" {
  type        = string
  description = "The local path to the directory containing all object/files"
}
