variable "prefix" {
  type        = string
  description = "The prefix for the Remote State"
  default     = "project-setup"
}

variable "bucket" {
  type        = string
  description = "The Bucket in which the Remote State is stored"
}

