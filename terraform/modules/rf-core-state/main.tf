data "terraform_remote_state" "core" {
  backend = "gcs"

  config = {
    prefix = var.prefix
    bucket = var.bucket
  }
}

locals {
  outputs = data.terraform_remote_state.core.outputs
}
