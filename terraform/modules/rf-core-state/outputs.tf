output "organization" {
  value = local.outputs.organization
}

output "project" {
  value = local.outputs.support.core.project
}

output "label" {
  value = local.outputs.support.core.label
}
