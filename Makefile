.PHONY: init plan deploy

PWD ?= $(shell 'pwd')

BACKEND_CONFIG ?= backend-config.tfvars
GC_PROJECT_ID ?= ""
GOOGLE_APPLICATION_CREDENTIALS ?= $(shell echo $(PWD))/google-application-credentials.json
GC_APPLICATION_CREDENTIALS_PATH ?= $(shell echo $(PWD))/$(shell echo $(GC_APPLICATION_CREDENTIALS))
MAKE_DIR ?= make
TF_OUT ?= "build-harness.tfplan"

BACKEND_CONFIG_PATH := $(shell echo $(PWD))/$(shell echo $(BACKEND_CONFIG))
MAKE_PATH := $(shell echo $(PWD))/$(shell echo $(MAKE_DIR))
TF_OUT_PATH := $(PWD)/$(TF_OUT)
TF_IN_AUTOMATION := $(if $(CI),$(true),)

include $(MAKE_PATH)/gmsl
include $(MAKE_PATH)/*.mk

login:
ifeq ($(GOOGLE_CREDENTIALS),)
	@echo "No Client Secret set, using: $(GOOGLE_APPLICATION_CREDENTIALS) contents";
else
	@printf "%s" '${GOOGLE_CREDENTIALS}' > "$(GOOGLE_APPLICATION_CREDENTIALS)";
endif
	@gcloud auth activate-service-account --key-file "$(GOOGLE_APPLICATION_CREDENTIALS)";

plan: login
	@make tf/plan \
		TF_VAR_FILES="$(PWD)/build-harness.tfvars" \
		TF_VARS='release_version="$(call get_current_rf_version)" dir_path="$(MAKE_PATH)"' \
		TF_BACKEND_CONFIG=$(BACKEND_CONFIG_PATH) \
		TF_OUT=$(TF_OUT_PATH);

deploy: login
	@make tf/apply \
		TF_BACKEND_CONFIG=$(BACKEND_CONFIG_PATH) \
		TF_OUT=$(TF_OUT_PATH);
