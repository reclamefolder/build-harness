# Reclamefolder Build Harness

This build-harness is a collection of Makefiles to facilitate building Reclamefolder projects

## Usage

- Within your project, remove from the root directory, the directory: `make`
- Add `/make` to the `.gitignore` from the project
- Ensure you have Google Cloud SDK installed (`wich gsutil`), otherwise on Mac install with: `curl https://sdk.cloud.google.com | bash`
- Create a Makefile with the following content and replace: `{VERSION}` with the wanted build-harness version:

```
MAKE_DIR ?= make
RF_BUILD_HARNESS_URL ?= gs://rf-build-harness
RF_BUILD_HARNESS_VERSION ?= {VERSION}

MAKE_PATH := $(shell echo $(PWD))/$(shell echo $(MAKE_DIR))

-include $(MAKE_PATH)/gmsl
-include $(MAKE_PATH)/*.mk

$(MAKE_DIR):
    @mkdir -p $(MAKE_PATH);
    @gsutil -m rsync -d -r "$(RF_BUILD_HARNESS_URL)/$(RF_BUILD_HARNESS_VERSION)/" "$(shell echo $(MAKE_PATH))"

init: $(MAKE_DIR)
    @echo "Initialized Make";
```

- Restart your terminal
- Run: `make init`
- Validate by running: `make deployment/display` or `make environment/display`


## Targets

### GCLOUD AppEngine

#### gc/ae/deploy

Depends on:
- deployments.mk
- reclamefolder.mk

```
> make gc/ae/deploy \
    GAE_CONFIG_DIR="app-engine" \
    GAE_PROJECT_ID="reclamefoldernl-eu" \
    GAE_VERSION="1.0.0"
```

| ENVIRONMENT VARIABLE | REQUIRED | DEFAULT |
| -------------------- | -------- | ------- |
| GAE_CONFIG_DIR | yes | app-engine |
| GAE_PROJECT_ID | yes | reclamefoldernl-eu |
| GAE_VERSION | yes | - |
| GAE_CONFIG | no | Deployment config |
| GAE_IMAGE_URL | no | - |
| DEPLOYMENT | yes | staging |
| DEPLOYMENT_TYPE | no | - |
| BRANCH | no | - |

##### Deployment config: `app.yaml`

Based on the DEPLOYMENT environment variable the first existing `app.yaml` is selected.

**${DEPLOYMENT} = production:**

- `app-production-${DEPLOYMENT_TYPE}-v${major version number}.yaml`
- `app-production-v${major version number}.yaml`
- `app-production-${DEPLOYMENT_TYPE}.yaml`
- `app-${BRANCH}.yaml`
- `app-production.yaml`
- `app.yaml`

**${DEPLOYMENT} = staging:**

- `app-staging-v${major version number}.yaml`
- `app-${BRANCH}.yaml`
- `app-staging.yaml`
- `app.yaml`

**${DEPLOYMENT} = acceptance:**

- `app-acceptance-v${major version number}.yaml`
- `app-${BRANCH}.yaml`
- `app-acceptance.yaml`
- `app.yaml`

**Other:**

- `app-${BRANCH}.yaml`
- `app-${DEPLOYMENT}.yaml`
- `app.yaml`
